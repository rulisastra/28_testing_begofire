# 28_testing_begofire

Pertemuan 28. testing

---
### how to use

<details>
<summary>"Lihat how"</summary>

- source code dari [be_go_mysql](https://gitlab.com/rulisastra/24_mvc_golang)
- open xampp, start mysql
- bikin db `create database digitalent_bankfire`
- CEK semua method di postman atau browser http://localhost:8084/customer/account
```
server.AssignHandler(`/guest/create-account`,cGuest.CreateAccount)
server.AssignHandler(`/guest/login`,cGuest.Login)
server.AssignHandler(`/customer/account`,cCustomer.Account)
server.AssignHandler(`/customer/transfer`,cCustomer.Transfer)
server.AssignHandler(`/customer/withdraw`,cCustomer.Widthdraw)
server.AssignHandler(`/customer/deposit`,cCustomer.Deposit)
server.Listen(":8084")
```
- buat file baru `main_test.go`
- install package `go get github.com/kokizzu/gotro/M` dan sync dengan `go mod vendor` (lama downloadnya)
-


You're doing an awesome work!
</details>

---

### tugas

<details>
<summary>"Lihat tugas"</summary>

Lakukan explorasi tool-tool unit testing atau API/UI/integration/load testing
(automated/software-based testing apapun bebas, yg penting jangan manual testing) buat PDF untuk mendemokan salah satu jenis testing terhadap API/modul/fungsi yang sudah kalian buat sebelumnya.
Screenshotnya kecil2 saja.

---

```
Contoh cara mengerjakan (misal memilih untuk melakukan go test dengan http.Client biasa):
func TestRegisterLoginDepositCheckSaldoApi() {
  c := http.Client{}
  // HIT API register dengan username random password random, check tidak boleh error
  // HIT API register dengan username yg sama lagi, check errornya
  // HIT API login dengan username benar, password salah, check harus error
  // HIT API login dengan username benar, password benar, check harus dapat token
  // HIT API deposit dengan token salah, check harus error
  // HIT API check saldo dengan token benar, check saldo mula2
  // HIT API deposit dengan token benar, check tidak boleh error
  // HIT API check saldo dengan token benar, check saldo harus bertambah
  // HIT API deposit dengan token benar, check tidak boleh error
  // HIT API check saldo dengan token benar, check saldo harus bertambah lagi dengan jumlah benar
}
```

You're doing an awesome work!
</details>

---

### Sumber belajar

- Kalau perlu source code API yg lama: [BackEnd Pak Kiswono - BE01gofire](https://github.com/kokizzu/be01gofire) 
- Fiddler https://www.telerik.com/fiddler --> bisa untuk replay request dari manual testing, jadi testing berikutnya jadi automated
- Cypress https://www.cypress.io/ --> bisa melakukan automated testing UI web frontend
- Go Test https://golang.org/pkg/testing/ | https://tutorialedge.net/golang/intro-testing-in-go/ --> unit testing
- Pprof https://github.com/google/pprof --> bisa untuk tracing performance (berapa persen processing/CPU masuk di bagian mana, berapa alokasi RAM yg dipakai per bagian, dll)
- Vegeta, wrk, k6, dll https://github.com/tsenart/vegeta --> bisa untuk performance testing
Heimdal https://github.com/gojek/heimdall --> bisa untuk melakukan hit API programatically, atau pakai net/http.Client atau curl juga bisa
- Gofuzz https://github.com/google/gofuzz --> menggenerate input invalid, untuk memastikan semua error terhandle dengan baik
- Mocha https://mochajs.org/ --> untuk melakukan unit testing di NodeJS
- Jest https://jestjs.io/ --> untuk melakukan unit testing javascript frontend
- Contoh cara hit API dengan http.Client untuk load testing: https://github.com/kokizzu/tpoints/blob/master/main_test.go 